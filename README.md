# Data-Engineer-Challenge

Objetivo
Esperamos conocer tus capacidades en manipulación de información.

Challenge
Deberas bajar el dataset de https://data.world/data-society/european-soccer-data y realizar una
limpieza de información.
https://en.wikipedia.org/wiki/Data_cleansing
Adicional a esto deberás entregar un análisis descriptivo de las siguientes columnas:
 País
 Liga
 Equipo
A partir de la fecha de que recibes el correo tendrás 6 días para entregar la prueba. Cualquier
consulta me escribes a guillermo.marin@banistmo.com.


Recomiendo isntalar Jupyter para una mejor visualizacion del proyecto

by: David Diaz
