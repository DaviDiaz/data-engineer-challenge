import pandas as pd
import sqlite3
import matplotlib.pyplot as plt

#Connecting Database
dataBase = sqlite3.connect("database.sqlite")

#get data frame
dfCountry = pd.read_sql_query("Select * from Country", dataBase)
dfLeague = pd.read_sql_query("Select * from League", dataBase)
dfMatch = pd.read_sql_query("Select * from Match", dataBase)
dfTeam = pd.read_sql_query("Select * from Team", dataBase)

#Detalle de Ligas por Paises
df = pd.concat(
    [dfCountry.set_index('id'),
     dfLeague.set_index('country_id')],
    axis=1, keys=('Country','League')
    )

#Evaluacion de equipo con mas goles
merged_inner = pd.merge(
    left=dfTeam,right=dfMatch,
    left_on='id', right_on='id')

dfGoal = merged_inner[
    ["team_long_name","home_team_goal","away_team_goal"]
    ]

def totalGoals(goals):
    sumGoals = goals["away_team_goal"] + goals["home_team_goal"]
    return sumGoals

dfGoal = dfGoal.copy()
dfGoal["total_goals"] = dfGoal.apply(totalGoals, axis=1)

grouped = dfGoal.groupby("team_long_name").sum()

#grouped

#Estadistica de los dos mejores goleadores
grouped[grouped["total_goals"] > 6].plot(kind="bar")

#El equipo con mas goles en casa
grouped[grouped["total_goals"] > 6]

#Podemos analizar teniendo una muestra de 155 datos, que hay mas posibilidades de meter mas
#goles en local. aparte en la desviacion no tenemos una gran dispersion de datos por ende el
#resultado con respecto a la media, podriamos decir que es mas exacto.
grouped.describe()
