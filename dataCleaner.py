import pandas as pd
import sqlite3

#Connecting Database
dataBase = sqlite3.connect("database.sqlite")

#get data frame
dfMatch = pd.read_sql_query("Select * from Match", dataBase)
dfPlayerAttributes = pd.read_sql_query("Select * from Player_Attributes", dataBase)
dfTeam = pd.read_sql_query("Select * from Team", dataBase)
dfTeamAttributes = pd.read_sql_query("Select * from Team_Attributes", dataBase)

#data cleaning
dfMatch[
    ["goal","shoton","shotoff",
    "foulcommit","card","cross",
    "corner""possession"]
    ] = 0
dfMatchCleaning = dfMatch.fillna(0)
dfPlayerAttributesCleaning = dfPlayerAttributes.fillna(0)
dfTeamCleaning = dfTeam.fillna(
    {"team_fifa_api_id": 0}
    )
dfTeamAttributesCleaning = dfTeamAttributes.fillna(
    dfTeamAttributes.buildUpPlayDribbling.mean())

#Update database
dfMatchCleaning.to_sql("Match", dataBase, if_exists="replace")
dfPlayerAttributesCleaning.to_sql("Player_Attributes", dataBase, if_exists="replace")
dfTeamCleaning.to_sql("Team", dataBase, if_exists="replace")
dfTeamAttributesCleaning.to_sql("Team_Attributes", dataBase, if_exists="replace")
